const cheerio = require('cheerio')
require('dotenv').config();
const fs = require('fs');
const request = require("request");
const IS_DEV = process.env.NODE_ENV == "development";
const TMP_PATH = IS_DEV ? "./tmp/" : "/tmp/"
const COOKIE_NAME = "PHPSESSID"
const USERNAME = process.env.MIKA_USERNAME
const PASSWORD = process.env.MIKA_PASSWORD
const NEWS_CACHE = "news_cache.json"

const debugHtml = fs.readFileSync('./samples/berita.html');
const debugJson = fs.readFileSync('./samples/berita.json');

// need to send login request first
const getLoginOptions = (rawCookie, username = USERNAME, password = PASSWORD) => {

  return rawCookie ? {
    'method': 'POST',
    'url': 'https://mika.mikroskil.ac.id/login_check',
    'headers': {
      'Referer': 'https://mika.mikroskil.ac.id',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': rawCookie
    },
    form: {
      '_username': username,
      '_password': password
    },
    followAllRedirects: true,
    jar: true,
  } : {
    'method': 'POST',
    'url': 'https://mika.mikroskil.ac.id/login_check',
    'headers': {
      'Referer': 'https://mika.mikroskil.ac.id',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    form: {
      '_username': username,
      '_password': password
    },
    followAllRedirects: true,
    jar: true,
  }

}

const login = (cookie, username = "'", password = "") =>
  new Promise((resolve, reject) => {
    request(getLoginOptions(cookie, username, password), (err, res) => {
      if (err) throw new Error(err)

      const rawCookie = res.request.headers['cookie']
      res.rawCookie = rawCookie;
      console.log(res.statusCode)
      resolve(res)
    })
  })

const checkOptions = (rawCookie) => ({
  'method': 'GET',
  'url': 'https://mika.mikroskil.ac.id/',
  'Cookie': rawCookie
});

const checkHomePage = (rawCookie) => new Promise((resolve, reject) => {
  return request(checkOptions(rawCookie), (reqErr, reqRes) => {
    if (reqErr) throw new Error(reqErr);
    else if (reqRes.statusCode == 200) {
      const $ = cheerio.load(reqRes.body)
      const title = $('head > title').text();
      resolve(title)
    } else {
      console.error("unknown error")
    }
  })
});

const checkLoginPage = (rawCookie) => new Promise((resolve, reject) => {
  return request(getLoginOptions(rawCookie), function (error, response) {
    if (error) {
      reject(error)
    } else if (response.request.headers['cookie']) {
      const rawCookie = response.request.headers['cookie']
      const cookie = rawCookie.substr(COOKIE_NAME.length + 1)
      const $$ = cheerio.load(response.body)
      if (response.statusCode == 200) {
        console.log($$(".error-message").text())
        const loginTitle = $$('head > title').text()
        if (loginTitle) resolve({ loginTitle, rawCookie, cookie })
        else reject("no login title!")
      } else {
        reject("\nError:\nExpected status code " + 200 +
          "\nReceived status code " + response.statusCode
          + "\nDid you leave the form blank?\n")
      }
    }
  })
})

const tryLogin = (rawCookie) =>
  new Promise((resolve, reject) => {
    return request(getLoginOptions(rawCookie), function (error, response) {
      if (error) {
        reject(error)
      } else if (response.request.headers['cookie']) {
        const rawCookie = response.request.headers['cookie']
        const cookie = rawCookie.substr(COOKIE_NAME.length + 1)
        const $ = cheerio.load(response.body)
        if (response.statusCode == 200) {
          console.log("Possible error: " + $(".error-message").text())
          if (!!$('.identitas-login-user').html()) resolve({ rawCookie, cookie })
          else reject("Identity element not found!")
        } else {
          reject("\nError:\nExpected status code " + 200 +
            "\nReceived status code " + response.statusCode
            + "\nDid you leave the form blank?\n")
        }
      }
    })
  })

const getNewSession = async (double = false, oldCookie) => {
  console.log("sending 1st login request..");

  let res = await login(oldCookie);
  if (double && res && res.statusCode != 200) {
    res = await login(oldCookie)
  }

  if (res && res.statusCode == 200) {
    const { rawCookie, cookie } = await tryLogin(res.cookie)
    return cookie;
  } else {
    throw new Error("Failed to get new sessionId\n")
  }
}

const getNews = (sessionId) => {
  return new Promise((resolve, reject) => {
    request("https://mika.mikroskil.ac.id/berita/", {
      'headers': {
        'Access-Control-Allow-Origin': true,
        'Cookie': `${COOKIE_NAME}=${sessionId}`
      },
      jar: true
    }, (aErr, aRes) => {
      const $ = cheerio.load(aRes.body)
      if (!$('.identitas-login-user').html()) {
        console.log("This cookie is invalid\n" +
          "Reason: User identity IS NOT detected in return page!")
        reject(sessionId)
      }

      if (aRes.statusCode == 200) resolve(aRes.body)
      else console.log("rejected!")
    })
  })
}

const retrieveNews = (sessionId) => {
  console.log(sessionId)
  return new Promise((resolve, reject) => {
    getNews(sessionId)
      .then((html) => {

        const $ = cheerio.load(html)
        const newsItems = [];

        $('.news-container').find('.news-item').each((i, elem) => {
          const title = $(elem).find('.news-title > a').text();
          const date = $(elem).find('.news-date').text().substring(3);
          const desc = $(elem).find('.news-description > span').text().replace(/^\s+|\s+$/g, '');
          const relativeUrl = $(elem).find('a').attr('href')
          const category = $(elem).find('.kategori').text();
          const newsItem = {
            title, date, desc, relativeUrl, category
          }

          newsItems.push(newsItem)
        })

        const formattedNewsItems = JSON.stringify(newsItems, null, 2)
        fs.writeFile(TMP_PATH + NEWS_CACHE, formattedNewsItems, (err) => {
          if (err) throw err;
          console.log('Data written to file');
        })
        resolve({ data: newsItems })
      })
      .catch((err) => {
        const oldCookie = err
        getNewSession(true, oldCookie)
          .then((sessionId) => {
            fs.writeFileSync(TMP_PATH + "session.txt", sessionId);
            retrieveNews(sessionId).then(v => {
              resolve(v)
            })
          })
          .catch(error => {
            console.log(error)
          })
      })
  })
}

function strToBool(s) {
  switch (s.toLowerCase()) { case "false": case "no": case "0": case "": return false; default: return true; }
}

exports.handler = async function (event) {

  // fetch file details
  let lastModifiedDate
  try {
    const stats = fs.statSync(TMP_PATH + NEWS_CACHE);

    // print file last modified date
    lastModifiedDate = stats.mtime
    console.log(`Cache Data Last Modified: ${lastModifiedDate}`);
  } catch (error) {
    console.log("file not found for stating");
  }

  let isCache = true;

  const isObjectEmpty = (obj) => (obj // 👈 null and undefined check
    && Object.keys(obj).length === 0
    && Object.getPrototypeOf(obj) === Object.prototype)

  if (typeof event === 'undefined') {
    var args = process.argv.slice(2);
    if (args.length) event = JSON.parse(args)
    else event = {}
  }
  if (!isObjectEmpty(event)) {
    isCache = event.hasOwnProperty('cache')
      ? event['cache'] : isCache
    isCache = strToBool(isCache)
  }

  if (isCache) {
    console.log("Trying to send cached news items");

    let cache;
    try {
      cache = fs.readFileSync(TMP_PATH + NEWS_CACHE)
    } catch {
      console.error("cache not found, retrieving new one instead")
      return getNewsItems()
    }

    cache = JSON.parse(cache)
    return {
      fetchedDate: lastModifiedDate,
      data: cache
    }

  } else {
    console.log("Trying to retrieve new news items");     
    return getNewsItems();
  }
  function sleep(ms = 5000) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async function getNewsItems() {

    let sessionId;
    try {
      sessionId = fs.readFileSync(TMP_PATH + "session.txt", "utf8")
    } catch (err) {
      sessionId = await getNewSession();
      fs.writeFileSync(TMP_PATH + "session.txt", sessionId);
      return retrieveNews(sessionId)
    }

    if (sessionId && sessionId != "") {
      return retrieveNews(sessionId)
    } else {
      sessionId = await getNewSession();
      fs.writeFileSync(TMP_PATH + "session.txt", sessionId);
      return retrieveNews(sessionId)
    }

  }
};
